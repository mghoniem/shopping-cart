package shoppingCart;

import exception.IlegalPriceException;
import exception.IlegalQuantityException;

public class CartItem implements ICartItem
{

    public int id;

    public int productId;

    public int quantity;

    public double price;
    
    public CartItem() {}
    /**
     * 
     * @param id:	item ID
     * @param productId	: product ID
     * @param quantity: quantity of product
     * @param price: price per product
     */
    public CartItem(int id, int productId, int quantity, double price)
    {
        this.id=id;
        this.productId=productId;
        this.quantity=quantity;
        this.price=price;
    }

    public int getId()
    {
        return id;
    }

    public int getProducstId()
    {
        return productId;
    }

    public int getQuantity()
    {
        return quantity;
    }

    public double getUnitPrice()
    {
        return price;
    }

    public double getTotalCost()
    {
        return quantity * price;
    }

    public void setId(int item_id)
    {
        id = item_id;
    }

    public void setProductId(int id)
    {
        productId = id;
    }

    public void setQuantity(int newQuantity)throws IlegalQuantityException
    {
        if (newQuantity <= 0)
        {
            throw new IlegalQuantityException("Quantity of product can't be less than or equal zero...");
        }
        quantity = newQuantity;
    }

    public void setUnitPrice(double unit_price)throws IlegalPriceException
    {
        if (unit_price <= 0)
        {
            throw new IlegalPriceException("Price of product can't be less than or equal zero.....");
        }
        price = unit_price;
    }

	public int getProductId() {
		return productId;
	}
}
