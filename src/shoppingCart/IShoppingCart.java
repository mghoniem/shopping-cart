package shoppingCart;

import java.sql.Date;
import java.util.ArrayList;

import exception.CartItemNotFoundException;
import exception.ItemAlreadyExistException;


public interface IShoppingCart {

  public int getId();

  public int getSessionID();
  
  public int getCustomerID();

  public void addItem(ICartItem item) throws ItemAlreadyExistException;

  public void updateQuantity(int cartItemID, int newQuantity) throws CartItemNotFoundException;

  public void removeItem(int cartItemId) throws CartItemNotFoundException;

  public ICartItem getItem(int productID) throws CartItemNotFoundException;

  public ArrayList<ICartItem>  getItems();

  public int countItems();

  public Date getLastAccessedDate();

}