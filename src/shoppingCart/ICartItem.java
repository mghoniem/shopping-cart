
package shoppingCart;

import exception.IlegalQuantityException;
import exception.IlegalPriceException;

public interface ICartItem
{
    public int getId();

    public int getProductId();

    public int getQuantity();

    public double getUnitPrice();

    public double getTotalCost();

    public void setId(int item_id);

    public void setProductId(int id);

    public void setQuantity(int id)throws IlegalQuantityException;

    public void setUnitPrice(double unit_price)throws IlegalPriceException;

}
