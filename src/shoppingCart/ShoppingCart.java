package shoppingCart;

import java.util.ArrayList;
import java.sql.Date;


import exception.*;

public class ShoppingCart implements IShoppingCart {
	
	  private int id;
	  private int sessionId;
	  private int customerId;
	  private ICartItem itemsArray;	
	  private Date lastAccessed;
	  private ArrayList<ICartItem> myICartItems;
	  
	  public ShoppingCart () {
		  myICartItems = new ArrayList<ICartItem>();
	  }
	  /**
	   * 
	   * @param cartID: shopping cart ID
	   * @param customerID: customer ID that own this cart
	   * @param sessionID: session ID when cart is created
	   */
	  public ShoppingCart(int cartID, int customerID, int sessionID) {
		  myICartItems = new ArrayList<ICartItem>();
		  id=cartID ; 
		  sessionId = sessionID;
		  customerId = customerID;
	  }
	
	  public void setID (int id ){
		  this.id = id ; 
	  }
	  
	  public int getId() {
		  return id;
	  }
	
	  public void setSessionID(int sessionId ){
		  this.sessionId  = sessionId ; 
	  }
	  
	  public int getSessionID() {
		  return sessionId;
	  }
	
	  public void setCustomerID(int customerId) {
		  this.customerId = customerId;
	  }
	  
	  public int getCustomerID() {
		  return customerId;
	  }
	  /**
	   * @param item: cart item 
	   * add new item in shopping cart
	   */
	  public void addItem(ICartItem item)throws ItemAlreadyExistException
	  {
		  boolean isExist= false;
		  int index = 0 ;
		  for (int i=0 ; i<myICartItems.size() ; i++ )
		  {
			  if ( myICartItems.get(i).getProductId() == item.getProductId() )
			  {
				  isExist=true;
				  index=i;
				  break;
			  }
		  }
		  if (!isExist)
			  myICartItems.add(item);
		  else 
		  {
			  throw new ItemAlreadyExistException("Cann't add Item "+item.getId()
					  +", "+"Product "+ item.getProductId()  
					  +" already exist in item " + myICartItems.get(index).getId());
		  }
		 
		  
	  }
	  /**
	   * @param ItemID: item ID 
	   * @param newQuantity: new quantity to be setted for Item with ID (ItemID)
	   * update quantity of product in it's Item 
	   */
	  public void updateQuantity(int cartItemID, int newQuantity)throws CartItemNotFoundException
	  {

		  int index= -1;
		  for (int i=0 ; i<myICartItems.size() ; i++ )
		  {
			  if ( myICartItems.get(i).getId() == cartItemID)
			  {
				  index=i;
				  break;
			  }
		  }
		  if (index >= 0)
		  {
			  try{
				  myICartItems.get(index).setQuantity(newQuantity);
			  }catch(IlegalQuantityException e){
				  System.out.println( e.getMessage() ) ;
			  }
		  }
		  else 
		  {
			  throw new CartItemNotFoundException("Cann't update quantity, Item "
					  								+ cartItemID +" not found");
		  }
	  }
	  /**
	   * @param ItemID: ID of item to be removed
	   * remove the whole Item from cart
	   */
	  public void removeItem(int cartItemId) throws CartItemNotFoundException
	  {
		  int index= -1;
		  for (int i=0 ; i<myICartItems.size() ; i++ )
		  {
			  if ( myICartItems.get(i).getId() == cartItemId)
			  {
				  index=i;
				  break;
			  }
		  }
		  if (index >= 0)
			  myICartItems.remove(index);
		  else 
		  {
			  throw new CartItemNotFoundException(("Cann't remove Item "+cartItemId
					  +", Item not found"));
		  }
	  }
	 /**
	  * @param Product ID: Id of product
	  * return Item of Product (ProductID) 
	  */
	  public ICartItem getItem(int productID)throws CartItemNotFoundException 
	  {
		  for (int i=0 ; i<myICartItems.size() ; i++ )
		  {
			  if ( myICartItems.get(i).getProductId() == productID)
			  {
				  ICartItem temp = new CartItem();
				  temp = myICartItems.get(i);
				  return temp;
			  }
		  }
		  throw new CartItemNotFoundException("Cann't get item, Item with productID "
				  								+ productID +" not found");
		  
	  }
	  
	  public ArrayList<ICartItem> getItems() {
		  return myICartItems;
	  }
	  /**
	   * return number of products in this shopping cart
	   */
	  public int countItems() {
		  int count=0;
		  for (int i=0 ; i<myICartItems.size() ; i++ )
		  {
			  count += myICartItems.get(i).getQuantity();
		  }
		  return count;
	  }
	
	  public Date getLastAccessedDate() {
		  return lastAccessed;
	  }
}