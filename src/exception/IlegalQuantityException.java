package exception;

public class IlegalQuantityException extends RuntimeException {
/**
 * 
 * @param msg  : msg of exception
 */
    public IlegalQuantityException(String msg) {
        super(msg);
    }
/**
 * 
 * @return msg of exception
 */
    @Override
    public String getMessage() {
        return super.getMessage();
    }

}