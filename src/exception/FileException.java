package exception;

public class FileException extends PersistenceException {
/**
 * 
 * @param msg  : msg of exception
 */
    public FileException(String msg) {
        super(msg);
    }

}
