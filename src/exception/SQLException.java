package exception;

public class SQLException extends PersistenceException {
/**
 * 
 * @param msg  : msg of exception 
 */
    public SQLException(String msg) {
        super(msg);
    }

}

