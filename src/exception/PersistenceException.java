package exception;

public class PersistenceException extends Exception {

/**
 * 
 * @param msg  : msg of exception
 */
    public PersistenceException(String msg) {
        super(msg);
    }
/**
 * 
 * @return msg of exception
 */
    @Override
    public String getMessage() {
        return super.getMessage();
    }

}
