package exception;

public class IlegalPriceException extends Exception {

 /**
  * 
  * @param msg : msg of exception
  */
    public IlegalPriceException(String msg) {
        super(msg);
    }
/**
 * 
 * @return exception msg
 */
    @Override
    public String getMessage() {

        return super.getMessage();
    }

}