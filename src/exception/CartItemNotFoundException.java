package exception;

public class CartItemNotFoundException extends Exception {
	/**
         * 
         * @param msg : msg of exception
         */
	public CartItemNotFoundException(String msg) {
        super(msg);
    }
/**
 * 
 * @return msg  of exception
 */
    @Override
    public String getMessage() {

        return super.getMessage();
    }

}