package exception;

public class ItemAlreadyExistException extends Exception {
    /**
     * 
     * @param msg  : msg of exception
     */
    public ItemAlreadyExistException(String msg) {
        super(msg);
    }
/**
 * 
 * @return msg of exception
 */
    @Override
    public String getMessage() {

        return super.getMessage();
    }
    
}
