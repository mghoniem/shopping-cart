package testShoppingCart;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class TestRunner {
	public static void main(String[] args) {
		Result result = JUnitCore.runClasses(ShoppinCartTest.class);
		for (Failure failure : result.getFailures()) {
				System.out.println(failure.toString());
		}
		
		Result result2 = JUnitCore.runClasses(CartItemTest.class);
		for (Failure failure : result2.getFailures()) {
				System.out.println(failure.toString());
		}
		
	}
}