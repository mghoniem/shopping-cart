package testShoppingCart;

import static org.junit.Assert.*;

import org.junit.Test;

import exception.CartItemNotFoundException;
import exception.ItemAlreadyExistException;
import shoppingCart.CartItem;
import shoppingCart.ICartItem;
import shoppingCart.IShoppingCart;
import shoppingCart.ShoppingCart;

public class ShoppinCartTest {

	@Test(expected = ItemAlreadyExistException.class)
	public void testAddItemExceptionIsThrown() {
		IShoppingCart tester = new ShoppingCart(); 
		ICartItem temp=new CartItem(1,201,2,10); // CartItem(int id, int productId, int quantity, double price)
		ICartItem temp2=new CartItem(2,201,2,10);
		
		try {
			tester.addItem(temp);
		} catch (ItemAlreadyExistException e) {
			System.out.println( e.getMessage() ) ;
		}
		try {
			tester.addItem(temp2);
		} catch (ItemAlreadyExistException e) {
			System.out.println( e.getMessage() ) ;
		}
	}
	@Test(expected = ItemAlreadyExistException.class)
	public void testUpdateQuantityExceptionIsThrown() {
		IShoppingCart tester = new ShoppingCart();
		ICartItem temp=new CartItem(1,201,2,10); // CartItem(int id, int productId, int quantity, double price)
		ICartItem temp2=new CartItem(2,202,2,10);
		
		try {
			tester.addItem(temp);
			tester.addItem(temp2);
		} catch (ItemAlreadyExistException e) {
			System.out.println( e.getMessage() ) ;
		}
		
		try {
			tester.updateQuantity(1,4);
		} catch (CartItemNotFoundException e) {
			System.out.println( e.getMessage() ) ;
		}
		
		try {
			tester.updateQuantity(3,6);
		} catch (CartItemNotFoundException e) {
			System.out.println( e.getMessage() ) ;
		}
	}

	@Test(expected = ItemAlreadyExistException.class)
	public void testRemoveItemExcepionIsThrown() {
		IShoppingCart tester = new ShoppingCart();
		ICartItem temp=new CartItem(1,201,2,10); // CartItem(int id, int productId, int quantity, double price)
		ICartItem temp2=new CartItem(2,202,2,10);
		
		try {
			tester.addItem(temp);
			tester.addItem(temp2);
		} catch (ItemAlreadyExistException e) {
			System.out.println( e.getMessage() ) ;
		}
		
		try {
			tester.removeItem(1);
		} catch (CartItemNotFoundException e) {
			System.out.println( e.getMessage() ) ;
		}
		try {
			tester.removeItem(3);
		} catch (CartItemNotFoundException e) {
			System.out.println( e.getMessage() ) ;
		}
	}

	@Test
	public void testCountItems() {
		IShoppingCart tester = new ShoppingCart();
		ICartItem temp=new CartItem(1,201,1000,10); // CartItem(int id, int productId, int quantity, double price)
		ICartItem temp2=new CartItem(2,202,1000,10);
		ICartItem temp3=new CartItem(3,204,1000000000,10);
		
		try {
			tester.addItem(temp);
			tester.addItem(temp2);
			tester.addItem(temp3);
		} catch (ItemAlreadyExistException e) {
			System.out.println( e.getMessage() ) ;
		}
		
		assertEquals("count must be 1000002000 ", 1000002000, tester.countItems());
		
		IShoppingCart tester2 = new ShoppingCart();
		
		try {
			tester2.addItem(new CartItem(1,201,2,10));
			tester2.addItem(new CartItem(2,202,2,10));
		} catch (ItemAlreadyExistException e) {
			System.out.println( e.getMessage() ) ;
		}
		
		assertEquals("count must be 4 ", 4, tester2.countItems());
	}

}
