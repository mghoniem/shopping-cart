package testPresistenceLayer;


import org.junit.Test;

import persistenceLayer.FilePersistence;
import persistenceLayer.IPersistenceMechanism;
import persistenceLayer.PersistenceFactory;
import persistenceLayer.SQLPersistence;

public class PersistenceFactoryTest {

	@Test
	public void testLoadMechanism() {
		PersistenceFactory tester = new PersistenceFactory();
		
		IPersistenceMechanism presisMechFile=null;
		presisMechFile =  tester.loadMechanism("FilePersistence");
		
		assert (!(presisMechFile instanceof FilePersistence))
				:"presisMechFile should be instance of FilePersistence.";
				
	
		IPersistenceMechanism presisMechSQL=null;
		presisMechSQL =  tester.loadMechanism("SQLPersistence");
		
		assert (!(presisMechSQL instanceof SQLPersistence))
				: "presisMechSQL should be instance of SQLPersistence.";
		
	}
}
