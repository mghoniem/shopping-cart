package testPresistenceLayer;


import java.sql.SQLException;

import org.junit.Test;

import persistenceLayer.*;
import shoppingCart.*;

public class SQLPersistenceTest {
	
	@Test
	public void testSave() {  
		PersistenceFactory factory = new PersistenceFactory();
		IPersistenceMechanism presisMechSQL;
		presisMechSQL = factory.loadMechanism("SQLPersistence");
		
		IShoppingCart cart = new ShoppingCart(1,20011,312);
		
		try {
			presisMechSQL.save(cart);
		} catch (ClassNotFoundException | SQLException e) {
			e.getMessage();
		}
		
	}

	@Test
	public void testRemoveCart() {
		PersistenceFactory factory = new PersistenceFactory();
		IPersistenceMechanism presisMechSQL;
		presisMechSQL = factory.loadMechanism("SQLPersistence");
		
		IShoppingCart cart = new ShoppingCart(1,20011,312);
		
		try {
			presisMechSQL.removeCart(cart);
		} catch (ClassNotFoundException | SQLException e) {
			e.getMessage();
		}
		
	}

	@Test
	public void testLoadCartInt() {
		PersistenceFactory factory = new PersistenceFactory();
		IPersistenceMechanism presisMechSQL;
		presisMechSQL = factory.loadMechanism("SQLPersistence");

		IShoppingCart cart = new ShoppingCart(1,20011,312);
		IShoppingCart tempCart =null;
		
		try {
			presisMechSQL.save(cart);
		} catch (ClassNotFoundException | SQLException e) {
			e.getMessage();
		}
		// test case 1
		try {
			tempCart = presisMechSQL.loadCart(1);
			assert (!(tempCart instanceof ShoppingCart)) : "loadCart() should return shoppingCart type";
		} catch (Exception e) {
			e.printStackTrace();
		}
		tempCart = null;
		// test case 2
		try {
			tempCart = presisMechSQL.loadCart(4);
			assert (!(tempCart instanceof ShoppingCart)) : "loadCart() should return shoppingCart type";
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Test
	public void testLoadCartIntInt() {
		PersistenceFactory factory = new PersistenceFactory();
		IPersistenceMechanism presisMechSQL;
		presisMechSQL = factory.loadMechanism("SQLPersistence");
		
		IShoppingCart cart = new ShoppingCart(1,20011,312);
		IShoppingCart tempCart =null;
		
		try {
			presisMechSQL.save(cart);
		} catch (ClassNotFoundException | SQLException e) {
			e.getMessage();
		}
		// test case 1
		try {
			tempCart = presisMechSQL.loadCart(312,20011);
			assert (!(tempCart instanceof ShoppingCart)) : "loadCart() should return shoppingCart type";
		} catch (Exception e) {
			e.printStackTrace();
		}
		tempCart =null;
		// test case 2
		try {
			tempCart = presisMechSQL.loadCart(3,20011);
			assert (!(tempCart instanceof ShoppingCart)) : "loadCart() should return shoppingCart type";
		} catch (Exception e) {
			e.printStackTrace();
		}
		tempCart =null;
		// test case 3
		try {
			tempCart = presisMechSQL.loadCart(312,211);
			assert (!(tempCart instanceof ShoppingCart)) : "loadCart() should return shoppingCart type";
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Test
	public void testCreateCart() {
		PersistenceFactory factory = new PersistenceFactory();
		IPersistenceMechanism presisMechSQL;
		presisMechSQL = factory.loadMechanism("SQLPersistence");
		
		IShoppingCart cart =presisMechSQL.createCart(20011, 301);
		
		assert (!(cart instanceof ShoppingCart)) : "createCart() should return shoppingCart type";
		
		
		
	}

}
