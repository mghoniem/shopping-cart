package persistenceLayer;
import shoppingCart.*;

import java.sql.*;
import java.util.ArrayList;

import exception.IlegalPriceException;
import exception.ItemAlreadyExistException;
import shoppingCart.IShoppingCart;

public class SQLPersistence implements IPersistenceMechanism {

	/**
	 * Connection connection Database connection 
 	 */
	public Connection connection ;
	
	/**
	 * IPersistenceMechanism instance
	 */
	public static IPersistenceMechanism instance;
	
	/**
	 *	SQLPersistence default constructor to instantiate the class SQLPersistence 
	 */
	private	SQLPersistence() {
	}	
	
	/**
	 * IPersistenceMechanism return a new instance from the SQLPersistence 
	 * @return void function
	 */
	public static IPersistenceMechanism getInstance() {
		if (instance == null )
			instance = new SQLPersistence();
		return instance;
	}
	
	/**
	 * save store the data of a given cart in the database 
	 * @param cart is a certain cart from the type IShoppingCart which delete it's data from the database
	 * @return this is a void function 
	 */			
	public void save(IShoppingCart cart) throws SQLException ,ClassNotFoundException{
	 	Class.forName("com.mysql.jdbc.Driver");
	    String serverName = "localhost:3306";
	    String mydatabase = "shoppingcart";
	    String url = "jdbc:mysql://" + serverName + "/" + mydatabase; 
	    String username = "root";
	    String password = "";
	    
	    connection = DriverManager.getConnection(url, username, password);
	    Statement stat = connection.createStatement() ; 
	    stat.executeUpdate("Insert Into sCart VALUES ('"+cart.getId()+"','"+cart.getSessionID()+"','"+cart.getCustomerID()+"')");
	    ArrayList<ICartItem> itemList = cart.getItems();
 	    for (int i = 0 ; i < itemList.size() ; ++i ){
 	    	stat.executeUpdate("Insert Into item VALUES ('"+itemList.get(i).getId()+"','"+itemList.get(i).getProductId()+"','"+itemList.get(i).getQuantity()+"',"+
 	    				"'"+itemList.get(i).getTotalCost()+"','"+cart.getId()+"')");
 	    }	    
	    connection.close();
	}
	/**
	 * removeCart remove from the database a stored cart using it's ID 
	 * @param cart is a certain cart from the type IShoppingCart which delete it's data from the database
	 * @return this is a void function 
	 */		
	public void removeCart(IShoppingCart cart) throws ClassNotFoundException , SQLException {
		 	Class.forName("com.mysql.jdbc.Driver");
		    String serverName = "localhost:3306";
		    String mydatabase = "shoppingcart";
		    String url = "jdbc:mysql://" + serverName + "/" + mydatabase; 
		    String username = "root";
		    String password = "";
		    
		    connection = DriverManager.getConnection(url, username, password);
		    Statement stat = connection.createStatement() ;
		    stat.executeUpdate("DELETE FROM scart WHERE sCart_Id = '"+cart.getId()+"' ");
		    
		    ArrayList<ICartItem> itemList = cart.getItems();
	 	    for (int i = 0 ; i < itemList.size() ; ++i ){
	 	    	stat.executeUpdate("DELETE FROM item WHERE item_shoppingCartID = '"+cart.getId()+"'");
	 	    }	    
		    connection.close();
		
	}
		
	/**
	 * loadCart search the database for a given cartID and get the data related to that IShoppingCart and return it 
	 * @param cartID User session id 
	 * @return IShoppingCart new object from IShoppingCart with data stored in the database
	 */		
	public IShoppingCart loadCart(int cartID) throws SQLException ,ClassNotFoundException , ItemAlreadyExistException , IlegalPriceException {
		ShoppingCart cart = new ShoppingCart() ; 
	 	Class.forName("com.mysql.jdbc.Driver");
	    String serverName = "localhost:3306";
	    String mydatabase = "shoppingcart";
	    String url = "jdbc:mysql://" + serverName + "/" + mydatabase; 
	    String username = "root";
	    String password = "";
	    
	    connection = DriverManager.getConnection(url, username, password);
	    Statement stat = connection.createStatement() ;
	    ResultSet res = stat.executeQuery("SELECT * FROM scart WHERE item_shoppingCartID = '"+cartID+"'");
	    res.next();
	    cart.setID(Integer.parseInt(res.getString("sCart_Id")));
	    cart.setCustomerID(Integer.parseInt(res.getString("sCart_customerId")));
	    cart.setSessionID(Integer.parseInt(res.getString("sCart_sessionId"))) ;
	    
	    res = stat.executeQuery("SELECT * FROM item WHERE item_shoppingCartID = '"+cartID+"'") ;
	    
	    while (res.next()){
	    	CartItem temp = new CartItem() ; 
	    	temp.setId( Integer.parseInt(res.getString("item_id")));
	    	temp.setProductId(Integer.parseInt(res.getString("item_productId")));
	    	temp.setQuantity(Integer.parseInt(res.getString("item_quantity")));
	    	temp.setUnitPrice(Integer.parseInt(res.getString("item_price")));
	    	cart.addItem(temp);
	    }
		return cart;
	}
	
	/**
	 * loadCart search the database for a given sessionID and customerID and get the data related to that IShoppingCart and return it 
	 * @param sessionID User session id 
	 * @param customerID Logged in customer id 	
	 * @return IShoppingCart new object from IShoppingCart with data stored in the database
	 */	
	public IShoppingCart loadCart(int sessionID, int customerID) {
		ShoppingCart cart = new ShoppingCart() ;
		try {
		 	Class.forName("com.mysql.jdbc.Driver");
		    String serverName = "localhost:3306";
		    String mydatabase = "shoppingcart";
		    String url = "jdbc:mysql://" + serverName + "/" + mydatabase; 
		    String username = "root";
		    String password = "";
		    
		    connection = DriverManager.getConnection(url, username, password);
		    Statement stat = connection.createStatement() ;
		    ResultSet res = stat.executeQuery("SELECT * FROM scart WHERE sCart_sessionId = '"+sessionID+"' AND sCart_customerId ='"+customerID+"'");
		    res.next();
		    cart.setID(Integer.parseInt(res.getString("sCart_Id")));
		    cart.setCustomerID(Integer.parseInt(res.getString("sCart_customerId")));
		    cart.setSessionID(Integer.parseInt(res.getString("sCart_sessionId"))) ;
		    res = stat.executeQuery("SELECT * FROM item WHERE item_shoppingCartID = '"+cart.getId()+"' ") ;  
		    while (res.next()){
		    	CartItem temp = new CartItem() ; 
		    	temp.setId( Integer.parseInt(res.getString("item_id")));
		    	temp.setProductId(Integer.parseInt(res.getString("item_productId")));
		    	temp.setQuantity(Integer.parseInt(res.getString("item_quantity")));
		    	temp.setUnitPrice(Integer.parseInt(res.getString("item_price")));
		    	cart.addItem(temp);
		    }
		}catch (Exception e ){
			
		}
		return cart;
	}	
	
	/**
	 * createCart to create a new intance from the shopping cart using the sessionID and customerID
	 * @param sessionID User session id 
	 * @param customerID Logged in customer id 	
	 * @return IShoppingCart new object from IShoppingCart  
	 */
	public IShoppingCart createCart(int sessionID, int customerID) {
		ShoppingCart cart = new ShoppingCart(123 , customerID , sessionID ) ; 
		return cart;
	}

}