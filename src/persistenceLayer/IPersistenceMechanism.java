package persistenceLayer;

import java.sql.SQLException;

import shoppingCart.*;

/*
 */
public interface IPersistenceMechanism {

  public void save(IShoppingCart cart) throws SQLException, ClassNotFoundException;

  public IShoppingCart loadCart(int cartID) throws Exception ;

  public IShoppingCart createCart(int sessionID, int customerID);

  public IShoppingCart loadCart(int sessionID, int customerID);

  public void removeCart(IShoppingCart cart) throws ClassNotFoundException, SQLException;

}