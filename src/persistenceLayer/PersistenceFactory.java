package persistenceLayer ;

import persistenceLayer.IPersistenceMechanism;

public class PersistenceFactory {
/**
 * 
 * @param type  : type of persistence
 * @return object from FilePersistence or SQLPersistence  
 */
  public IPersistenceMechanism loadMechanism(String type) {
      IPersistenceMechanism object;
      if(type.equals("FilePersistence")){
          object =FilePersistence.getInstance();
       return object;   
      }
      else if(type.equals("SQLPersistence")){
          object =SQLPersistence.getInstance();
          return object;
      }
      
  return null;
  }

}