package persistenceLayer;

import shoppingCart.IShoppingCart;
import shoppingCart.ShoppingCart;

public class FilePersistence implements IPersistenceMechanism {
	
	/**
	 * IPersistenceMechanism instance
	 */
	public IPersistenceMechanism instance;
	
	/**
	 *	FilePersistence default constructor to instantiate the class SQLPersistence 
	 */
	private FilePersistence() {
		
	}
	
	/**
	 * save store the data of a given cart in the database 
	 * @param cart is a certain cart from the type IShoppingCart which delete it's data from the database
	 * @return this is a void function 
	 */		
	public void save(IShoppingCart cart) {
		System.out.println("Cart has been saved ");
	}
	
	
	/**
	 * loadCart search the database for a given sessionID and customerID and get the data related to that IShoppingCart and return it 
	 * @param sessionID User session id 
	 * @param customerID Logged in customer id 	
	 * @return IShoppingCart new object from IShoppingCart with data stored in the database
	 */	
	public IShoppingCart loadCart(int cartID) {
		IShoppingCart cart = new ShoppingCart(cartID,12,123) ; 
		return cart;
	}
	
	
	/**
	 * createCart to create a new intence from the shopping cart using the sessionID and customerID
	 * @param sessionID User session id 
	 * @param customerID Logged in customer id 	
	 * @return IShoppingCart new object from IShoppingCart  
	 */
	public IShoppingCart createCart(int sessionID, int customerID) {
		IShoppingCart cart = new ShoppingCart(135,sessionID,customerID) ; 
		return cart;
	}
	
	/**
	 * removeCart remove from the database a stored cart using it's ID 
	 * @param cart is a certain cart from the type IShoppingCart which delete it's data from the database
	 * @return this is a void function 
	 */		
	public void removeCart(IShoppingCart cart) {
		System.out.println("Cart has been removed ");
	}
	
	public static IPersistenceMechanism getInstance() {
		return null;
	}
	
	/**
	 * loadCart search the database for a given sessionID and customerID and get the data related to that IShoppingCart and return it 
	 * @param sessionID User session id 
	 * @param customerID Logged in customer id 	
	 * @return IShoppingCart new object from IShoppingCart with data stored in the database
	 */	
	public IShoppingCart loadCart(int sessionID, int customerID) {
		return null;
	}
}