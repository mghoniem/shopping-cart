package webserver;

import java.util.ArrayList;
import java.util.Scanner;
import exception.*;
import persistenceLayer.*;
import shoppingCart.*;

public class WebServer {
	
	private static Scanner scanner;

	public static void displayMenu (){
		System.out.println("Products List ");
		System.out.println("1-Laptop hp , Quantity = 3 , price = 5700 LE ");
		System.out.println("2-Samsung Galaxy SIV , Quantity = 7 , price = 6200 LE ");
		System.out.println("Enter the product number & quantity to add to cart");
	}
	
	public static void main (String args[] ) throws  Exception{
		
		int pNumber = 0 , quantity = 0 ; 
		scanner = new Scanner(System.in);
		
		
		IPersistenceMechanism persistence = null ; 
		PersistenceFactory factory = new PersistenceFactory() ; 
		persistence = factory.loadMechanism("SQLPersistence");
		
		IShoppingCart cart =  persistence.createCart(1234, 12500) ; 
		displayMenu(); 
		
		
		pNumber = scanner.nextInt() ;
		quantity = scanner.nextInt() ; 
		
		
		CartItem item = new CartItem() ; 
		item.setId(785) ;
		item.setUnitPrice(5700) ; 
		item.setProductId(120);
		item.setQuantity(quantity); 
		
		cart.addItem(item);
		
		System.out.println("Assuming updates in quantity," +
							"User choosed a new quantity  ");
		quantity = scanner.nextInt() ; 
		cart.updateQuantity(item.getId(), quantity) ;
				
		
		System.out.println("Display item info " + item.getId());
		ICartItem newItem = cart.getItem(item.getProductId());
		
		System.out.println(newItem.getProductId());
		System.out.println(newItem.getQuantity());
		System.out.println(newItem.getUnitPrice());
			
		System.out.println("Display all items exist in shopping cart ");
		ArrayList<ICartItem> allItems = cart.getItems() ; 
			
		for (ICartItem current : allItems){
			System.out.print(current.getProductId() + " || " );
			System.out.print(current.getQuantity()+ " || ");
			System.out.print(current.getUnitPrice() + " || " );
			System.out.println(current.getQuantity());
		}
		
		System.out.println("Assuming user didn't" +
				" want the product,Deleting operation  ");
		cart.removeItem(item.getId());
		
		System.out.println("Saving Cart ");	
		persistence.save(cart);
		
		System.out.println("Cart loaded from database");
		IShoppingCart newCart =  persistence.loadCart(cart.getId());
	}
}







