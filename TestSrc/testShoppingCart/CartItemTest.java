package testShoppingCart;


import org.junit.Test;

import exception.IlegalPriceException;
import exception.IlegalQuantityException;
import shoppingCart.CartItem;
import shoppingCart.ICartItem;

public class cartItemTest {

	@Test
	public void testSetQuantity() {
		ICartItem tester = new CartItem();
		
		
		try {
			tester.setQuantity(0);
		} catch (IlegalQuantityException e) {
			System.out.println( e.getMessage() ) ;
		}
		try {
			tester.setQuantity(-222);
		} catch (IlegalQuantityException e) {
			System.out.println( e.getMessage() ) ;
		}
		try {
			tester.setQuantity(5 );
		} catch (IlegalQuantityException e) {
			System.out.println( e.getMessage() ) ;
		}
	}

	@Test
	public void testSetUnitPrice() {
ICartItem tester = new CartItem();
		
		
		try {
			tester.setUnitPrice(0);
		} catch (IlegalPriceException e) {
			System.out.println( e.getMessage() ) ;
		}
		try {
			tester.setUnitPrice(-55);
		} catch (IlegalPriceException e) {
			System.out.println( e.getMessage() ) ;
		}
		try {
			tester.setUnitPrice(87);
		} catch (IlegalPriceException e) {
			System.out.println( e.getMessage() ) ;
		}
	}

}
